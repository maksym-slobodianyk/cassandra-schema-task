class BadKeySpaceNameError extends Error {
    constructor(message) {
        super(message);
        this.name = 'BadKeySpaceNameError'
    }
}

module.exports = BadKeySpaceNameError
