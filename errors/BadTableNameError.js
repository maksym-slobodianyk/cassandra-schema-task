class BadTableNameError extends Error {
    constructor(message) {
        super(message);
        this.name = 'BadTableNameError'
    }
}

module.exports = BadTableNameError
