class BadSchemaError extends Error {
    constructor(message) {
        super(message);
        this.name='BadSchemaError'
    }
}

module.exports = BadSchemaError
