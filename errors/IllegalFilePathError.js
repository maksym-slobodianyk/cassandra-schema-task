class IllegalFilePathError extends Error {
    constructor(message) {
        super(message);
        this.name='IllegalFilePathError'
    }
}

module.exports = IllegalFilePathError
