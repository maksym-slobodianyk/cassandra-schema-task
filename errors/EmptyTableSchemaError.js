class EmptyTableSchemaError extends Error {
    constructor(message) {
        super(message);
        this.name = 'EmptyTableSchemaError'
    }
}

module.exports = EmptyTableSchemaError
