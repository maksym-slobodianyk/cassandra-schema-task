const dbModule = require('./task1App/database')
const schemaProcessor = require('./task2App/jsonProcessor')
const fileHelper = require('./task2App/fileHelper')

async function start() {
    await dbModule.connectToDB()
    const queries = await schemaProcessor.importSchema()
    await fileHelper.writeQueriesToFile(queries)
    await dbModule.multiExecute(queries)
    dbModule.shutdown()
}

start()
