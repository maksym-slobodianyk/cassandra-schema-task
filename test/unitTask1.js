const writeModule = require('../task1App/jsonSchemaFileWriter')
const writeJSONSchemaToFile = writeModule.write
const dbModule = require('../task1App/database')
const processor = require('../task1App/processor')
const assert = require('chai').assert;
const mocha = require("mocha")
const BadSchemaException = require('../errors/BadSchemaError')
const IllegalFilePathError = require('../errors/IllegalFilePathError')
const fs = require('fs');

describe('#writeJSONSchemaToFile()', function () {

    context('file write without arguments', function () {

        it('should throw IllegalFilePathException', function () {
            assert.throws(writeJSONSchemaToFile, IllegalFilePathError)
        })

        it('should throw BadSchemaException', function () {
            assert.throws(function () {
                writeJSONSchemaToFile("file.json");
            }, BadSchemaException)
        })

    })

    context('file write with arguments', function () {
        const testFileName = "test-file.json"
        const testSchema = {mock: "test schema"}

        it('should not throw Error', function () {
            assert.doesNotThrow(function () {
                writeJSONSchemaToFile(testFileName, testSchema);
                fs.unlink(testFileName, (err
                ) => {
                    if (err) console.log(err)
                })
            }, Error)
        })

        it('should write schema correctly', function () {
            writeJSONSchemaToFile(testFileName, testSchema);
            fs.readFile(testFileName, 'utf8', (err, content) => {
                assert.isTrue(JSON.parse(content).mock === testSchema.mock)
                fs.unlink(testFileName, (err
                ) => {
                    if (err) console.log(err)
                })
            })
        })
    })

})


describe('#exportSchema()', function () {

    context('schema creation', function () {

        const keySpaceCreationQuery = `CREATE KEYSPACE IF NOT EXISTS society WITH REPLICATION ={'class' : 'SimpleStrategy', 'replication_factor' : 1 }`
        const udtCreationQuery = `CREATE TYPE IF NOT EXISTS  society.basic_info (  birthday timestamp,  nationality text,  weight text,  height text);`
        const tableCreationQuery = `CREATE TABLE IF NOT EXISTS society.man
                                       (
                                          man_id             int PRIMARY KEY,
                                          man_name           text,
                                          man_address        tuple<text,text,int>,
                                          man_sal            varint,
                                          info               basic_info,
                                          man_children_names list<text>,
                                          account            text,
                                          is_employed        boolean,
                                      );`
        const insertQuery = `INSERT INTO society.man (man_id, man_name, man_address, man_sal, info, man_children_names, account, is_employed)
                                          VALUES (1, 
                                          'Joe',
                                          ('Kyiv', 'Uzviz', 2),
                                          3000,
                                          { birthday : '1993-06-18', nationality : 'New Zealand', weight : 'fifty kilograms', height : 'one hundred and seventy centimeters' },
                                          ['Karl','Julia'],
                                          '{ "Id": 1, "Player": {"nick":"Joe_Farmer","isReady":true,"achievements":["First plant","Farmer of the year"]} }',
                                          true);`

        it('should generate valid schema', async function () {
            await dbModule.connectToDB()
            await dbModule.execute(keySpaceCreationQuery)
            await dbModule.execute(udtCreationQuery)
            await dbModule.execute(tableCreationQuery)
            await dbModule.execute(insertQuery)
            const content = await processor.exportSchema(true)
            await fs.readFile('test/expectedGeneratedSchema.json', 'utf8', (err, json) => {
                const checkValue = json.replace(/\s/g,"")
                const currentValue = JSON.stringify(content.society[0])
                assert.isTrue(checkValue === currentValue)
            })
            await dbModule.execute("DROP  keyspace society")
            dbModule.shutdown()
        })

    })
})
