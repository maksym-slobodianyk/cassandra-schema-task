const assert = require('chai').assert;
const BadSchemaException = require('../errors/BadSchemaError')
const schemaProcessor = require('../task2App/jsonProcessor')
const fs = require('fs');

describe('#generateCQLFromSChema()', function () {

    context('UDT process', function () {

        it('Should generate query correctly', function () {
            const udtBody = {
                type: 'object',
                properties: {
                    id: {type: 'string'},
                    name: {type: 'number'},
                    arr: {
                        type: 'array',
                        uniqueItems: true,
                        items: {type: 'object', additionalProperties: {type: "number"}}
                    }
                }
            }
            const expected = 'CREATE TYPE IF NOT EXISTS  myspace.custom_udt ("id" text,"name" double,"arr" frozen<set<frozen<map<text,double>>>>);'
            const result = schemaProcessor.processUdtObject('custom_udt', udtBody, 'myspace')
            assert.equal(expected, result)
        })

    })

    context('CQL generation', function () {

        it('Should generate valid cql', function () {
            const schema = {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "title": "user",
                "properties": {
                    "id": {
                        "type": "string"
                    },
                    "name": {
                        "type": "number"
                    },
                    "surname": {
                        "$ref": "#/definitions/custom_udt"
                    },
                    "listSet": {
                        "type": "array",
                        "uniqueItems": false,
                        "items": {
                            "type": "array",
                            "uniqueItems": true,
                            "items": {
                                "type": "number"
                            }
                        }
                    },
                    "address": {
                        "type": "object",
                        "additionalProperties": {
                            "type": "number"
                        }
                    }
                },
                "definitions": {
                    "custom_udt": {
                        "type": "object",
                        "properties": {
                            "id": {
                                "type": "string"
                            },
                            "name": {
                                "type": "number"
                            },
                            "arr": {
                                "type": "array",
                                "uniqueItems": true,
                                "items": {
                                    "type": "object",
                                    "additionalProperties": {
                                        "type": "number"
                                    }
                                }
                            }
                        }
                    }
                }
            }
            const expectedQueries = [
                `CREATE KEYSPACE IF NOT EXISTS myspace WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor' : 1} AND DURABLE_WRITES = false;`,
                `USE "myspace";`,
                `CREATE TYPE IF NOT EXISTS  myspace.custom_udt ("id" text,"name" double,"arr" frozen<set<frozen<map<text,double>>>>);`,
                `CREATE TABLE IF NOT EXISTS myspace.user("id" text, "name" double, "surname" frozen<custom_udt>, "listSet" frozen<list<frozen<set<double>>>>, "address" frozen<map<text,double>>, PRIMARY KEY ("id"));`
            ]
            const result = schemaProcessor.parseSchema(schema);
            for (let i = 0; i < 4; i++)
                assert.equal(expectedQueries[i], result[i])
        })

    })

})
