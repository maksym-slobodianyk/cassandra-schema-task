const dbModule = require('./task1App/database')
const processor = require('./task1App/processor')
const jsonSchemaFileWriter = require('./task2App/fileHelper')

async function start() {
    await dbModule.connectToDB()
    const content = await processor.exportSchema()
    jsonSchemaFileWriter.writeJSONSchemaToFile("result.json", content)
    dbModule.shutdown()
}

start()

