const chalk = require('chalk')
const config = require('../config/config.js');
const BadKeySpaceNameError = require('../errors/BadKeySpaceNameError');
const BadTableNameError = require('../errors/BadTableNameError');
const EmptyTableSchemaError = require('../errors/EmptyTableSchemaError');
const dbModule = require('../task1App/database')
const jsonSchemaFileReader = require('./fileHelper')
const BadSchemaError = require("../errors/BadSchemaError");

async function importSchema() {
    try {
        const schema = await jsonSchemaFileReader.readJSONSchemaFromFile()
        const queries = parseSchema(schema)
        return queries;
    } catch (err) {
        console.log(chalk.red(`\n[x] Error during generating database schema`))
        console.log(`Error: ${err.message}\n`)
        throw err
    }
}

function parseSchema(schema) {
    const nameRegExp = /^[a-z\d]\w*$/;
    const keySpaceName = global.gDBConfig.keyspace
    const tableName = schema.title
    if (!nameRegExp.test(keySpaceName)) {
        console.log(chalk.red(`\n[x] Invalid keyspace name was entered in configs`))
        throw new BadKeySpaceNameError('Invalid keyspace name')
    }

    if (!nameRegExp.test(tableName)) {
        console.log(chalk.red(`\n[x] Invalid table name`))
        throw new BadTableNameError('Invalid table name')
    }
    const queries = [`CREATE KEYSPACE IF NOT EXISTS ${keySpaceName} WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor' : 1} AND DURABLE_WRITES = false;`,
        `USE "${keySpaceName}";`]
    if (schema.definitions !== undefined) {
        for (const def of Object.keys(schema.definitions)) {
            queries.push(processUdtObject(def, schema.definitions[def], keySpaceName))
        }
    }

    const fields = processTableProperties(schema.properties)
    queries.push(`CREATE TABLE IF NOT EXISTS ${keySpaceName}.${tableName}(${fields.join(', ')});`)
    return queries
}

function processProperties(properties) {
    const columnDefinitions = []

    if (Object.keys(properties).length === 0) {
        console.log(chalk.red(`\n[x] Table in schema is empty`))
        throw new EmptyTableSchemaError('Table in schema is empty')
    }

    for (prop in properties) {
        let type = processPropsBody(properties[prop])
        columnDefinitions.push(`"${prop}" ${type}`)
    }

    return columnDefinitions
}

function processTableProperties(properties) {
    const props = processProperties(properties)
    props.push(`PRIMARY KEY ("${Object.keys(properties)[0]}")`)

    return props
}

function processPropsBody(body) {
    if (body["$ref"] !== undefined) {
        if (/#\/definitions\/.+/.test(body["$ref"])) {
            return `frozen<${body["$ref"].replace("#/definitions/", "")}>`
        }
    }
    if (body['type'] == undefined) return "undefined"
    switch (body["type"]) {
        case 'string':
            if (body["format"] === undefined)
                return 'text'
            switch (body["format"]) {
                case 'date-time':
                    return "timestamp"
                case 'date': //from json schema draft7
                    return "date"
                case 'time': //from json schema draft7
                    return "time mp"
                case 'base64':
                    return "blob"
                default:
                    return 'text'
            }
        case 'number':
            return 'double'

        case 'boolean':
            return 'boolean'

        case 'array':
            let collectionType = ``
            if (body["uniqueItems"] !== undefined && body["uniqueItems"] === true)
                collectionType = 'set'
            else
                collectionType = 'list'

            if (body["items"] === undefined) {
                console.log(chalk.red(`\n[x] No property items in field of type array`))
                throw new BadSchemaError('No property items in field of type array')
            }
            const keys = Object.keys(body["items"])
            if (!keys.includes('type') && (keys.includes('anyOf') || keys.includes('allOf') || keys.includes('oneOf') || keys.includes('not'))) {
                console.log(chalk.yellow(`\n[i] Ignoring [allOf, anyOf, oneOf, not], set text type instead`))
                return `frozen<${collectionType}<text>>`
            }
            return `frozen<${collectionType}<${processPropsBody(body["items"])}>>`
        case 'object':
            if (body["additionalProperties"] !== undefined)
                if (body["additionalProperties"]["type"] !== undefined)
                    return `frozen<map<text,${processPropsBody(body["additionalProperties"])}>>`
            if (body["properties"] === undefined)
                return `frozen<map<text,text>>`
            //JSON
            return "text"
        default:
            return "text"
    }
}

function processUdtObject(name, body, keySpaceName) {
    return `CREATE TYPE IF NOT EXISTS  ${keySpaceName}.${name} (${processProperties(body.properties)});`
}

module.exports = {
    importSchema: importSchema,
    processUdtObject: processUdtObject,
    parseSchema: parseSchema
};
