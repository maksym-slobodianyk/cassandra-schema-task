const fs = require('fs');
const chalk = require('chalk')
const IllegalFilePathError = require('../errors/IllegalFilePathError')
const BadSchemaError = require('../errors/BadSchemaError')

async function readJSONSchemaFromFile() {
    if (!schemaFilePathIsCorrect()) {
        console.log(chalk.red(`\n[x] Invalid schema file path`))
        throw new IllegalFilePathError("Invalid schema file path")
    }
    let schema
    try {
        schema = JSON.parse(fs.readFileSync(global.gDBConfig.jsonSchema, 'utf8'));
        console.log(chalk.green(`\n[v] Schema is valid`))
    } catch (err) {
        console.log(chalk.red(`\n[x] Schema is not valid`))
        throw new IllegalFilePathError("Error during parsing input schema")
    }

    return schema
}

async function writeQueriesToFile(queries) {
    const path = global.gDBConfig.cqlScriptOutput

    if (!cqlFilePathIsCorrect()) {
        console.log(chalk.red(`\n[x] Invalid output sql script file path`))
        throw new IllegalFilePathError("Invalid output sql script file path")
    }

    fs.writeFile(path, queries.join("\n"), (err) => {
        if (err) {
            console.log(chalk.red(`\n[x] Can not write to file`))
            console.log(`\tError: ${err.message}\n`)
        } else {
            console.log(chalk.green(`\n[v] CQL script exported\n`))
        }
    })
}

function writeJSONSchemaToFile(path, schema) {
    if (path === undefined || path === "")
        throw new IllegalFilePathError("Bad path was passed")
    if (schema === undefined)
        throw new BadSchemaError("No schema was passed")
    fs.writeFile(path, JSON.stringify(schema, undefined, 4), (err) => {
        if (err) {
            console.log(chalk.red(`\n[x] Can not write to file`))
            console.log(`\tError: ${err.message}\n`)
        } else {
            console.log(chalk.green(`\n[v] Schema exported\n`))
        }
    })
}

function schemaFilePathIsCorrect() {
    let fileNameRegexp;
    //Basic check on filename
    //expected to work only on Windows Mac and Linux
    if (process.platform === "win32") { // Windows
        fileNameRegexp = /^[^\/\ :*?"<>|]*\.json$/
    } else { // Mac or Linux
        fileNameRegexp = /^[^:]*\.json$/
    }
    if (fileNameRegexp.test(global.gDBConfig.jsonSchema)) {
        return true
    }
    return false
}


function cqlFilePathIsCorrect() {
    let fileNameRegexp;
    //Basic check on filename
    //expected to work only on Windows Mac and Linux
    if (process.platform === "win32") { // Windows
        fileNameRegexp = /^[^\/\ :*?"<>|]*\.cql$/
    } else { // Mac or Linux
        fileNameRegexp = /^[^:]*\.cql$/
    }
    if (fileNameRegexp.test(global.gDBConfig.cqlScriptOutput)) {
        return true
    }
    return false
}

module.exports = {
    readJSONSchemaFromFile: readJSONSchemaFromFile,
    writeQueriesToFile: writeQueriesToFile,
    writeJSONSchemaToFile: writeJSONSchemaToFile
};
