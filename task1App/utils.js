function isNumeric(value) {
    return !isNaN(value)
}

function addValueToListInMap(map, key, value) {
    map[key] = map[key] || [];
    map[key].push(value);
}

function getColumnTypeByCode(code) {
    switch (code) {
        case 0x000a:
            return ({type: "text"})
        case 0x0001:
        case 0x000d:
        case 0x000c:
        case 0x000f:
        case 0x0010:
            return ({type: "string"})
        case 0x0002:
        case 0x0005:
        case 0x0006:
        case 0x0007:
        case 0x0008:
        case 0x0009:
        case 0x000e:
        case 0x0013:
        case 0x0014:
            return ({type: "number"})
        case 0x0004:
            return ({type: "boolean"})
        case 0x0020:
        case 0x0022:
            return ({type: "array"})
        case 0x0021:
            return ({type: "map"})
        case 0x0031:
            return ({type: "tuple"})
        case 0x0030:
            return ({type: "udt"})
        case 0x000b:
            return ({type: "timestamp"})
        case 0x0011:
            return ({type: "date"})
        case 0x0012:
            return ({type: "time mp"})
        case 0x0003:
            return ({type: "blob"})
        default:
            return ({type: "undefined"})
    }
}

const defaultKeySpaces = ['system_auth','system_schema','system_distributed','system','system_traces']

module.exports = {
    addValueToListInMap,
    isNumeric,
    getColumnTypeByCode,
    defaultKeySpaces
};
