const fs = require('fs');
const chalk = require('chalk')
const BadSchemaError = require('../errors/BadSchemaError')
const IllegalFilePathError = require('../errors/IllegalFilePathError')

function writeJSONSchemaToFile(path, schema) {
    if (path === undefined || path === "")
        throw new IllegalFilePathError("Bad path was passed")
    if (schema === undefined)
        throw new BadSchemaError("No schema was passed")
    fs.writeFile(path, JSON.stringify(schema, undefined, 4), (err) => {
        if (err) {
            console.log(chalk.red(`\n[x] Can not write to file`))
            console.log(`\tError: ${err.message}\n`)
        } else {
            console.log(chalk.green(`\n[v] Schema exported\n`))
        }
    })
}

module.exports = {
    write: writeJSONSchemaToFile
};
