const chalk = require('chalk')
const utilsModule = require('./utils')
const addValueToListInMap = utilsModule.addValueToListInMap
const defaultKeySpaces = utilsModule.defaultKeySpaces;
const BadKeySpaceNameError = require('../errors/BadKeySpaceNameError');
const dbModule = require('./database')
const {isNumeric} = require("./utils");
const cassandraTypes = require('cassandra-driver').types.dataTypes

async function exportSchema(testing = false) {
    const keySpaceName = global.gDBConfig.keyspace
    try {
        if (testing || keySpaceName === '') {

            const keySpaceTableNameMap = await getKeySpaceTableNameMap()
            const obj = await getKeySpaceTableSchemaMap(keySpaceTableNameMap)

            return obj;
        } else {
            const keySpaceNameRegExp = /^[a-z\d]\w*$/;

            if (!keySpaceNameRegExp.test(keySpaceName)) {
                console.log(chalk.red(`\n[x] Invalid keyspace name was entered in configs`))
                throw new BadKeySpaceNameError(`Invalid keyspace name was entered in configs`)
            }

            let keySpaceTables = await getKeySpaceTables(keySpaceName)
            keySpaceTables = keySpaceTables.rows
            const map = new Map()
            map.set(keySpaceName, keySpaceTables)

            return await getKeySpaceTableSchemaMap(map)
        }

    } catch (err) {
        console.log(chalk.red(`\n[x] Error during generating database schema`))
        console.log(`Error: ${err.message}\n`)
        throw err
    }
}

async function getKeySpaceTableNameMap() {
    const map = new Map()

    try {
        const keySpaces = Object.keys(dbModule.metadata().keyspaces).filter(k => !defaultKeySpaces.includes(k));

        for (keySpace of keySpaces) {
            map.set(keySpace, (await getKeySpaceTables(keySpace)).rows)
        }

    } catch (err) {
        console.log(chalk.red(`\n[x] Can not get table names from database`))
        console.log(`Error: ${err.message}\n`)
        throw err
    }

    return map;
}

async function getKeySpaceTables(keySpaceName) {
    const getTablesQuery = `SELECT table_name FROM system_schema.tables WHERE keyspace_name = '${keySpaceName}'`
    return await dbModule.execute(getTablesQuery)
}

async function getKeySpaceTableSchemaMap(keySpaceTableNameMap) {
    const keySpaceTableSchemaMap = new Map()

    for (const [key, value] of keySpaceTableNameMap.entries())
        for (let table of value) {
            const tableSchema = await getTableSchema(key, table['table_name'])
            addValueToListInMap(keySpaceTableSchemaMap, key, tableSchema)
        }

    return keySpaceTableSchemaMap
}

async function getTableSchema(keySpaceName, tableName) {
    const getColumnsQuery = `SELECT column_name, type FROM system_schema.columns WHERE keyspace_name = '${keySpaceName}' AND table_name = '${tableName}'`
    const definitionNames = new Set()
    const tableSchema = {
        $schema: "http://json-schema.org/draft-04/schema#",
        type: 'object',
        title: tableName,
        properties: {}
    }

    const columns = (await dbModule.execute(getColumnsQuery)).rows.map(r => ({
        name: r.column_name,
        type: parseCassandraType(r.type)
    }))
    for (let column of columns) {
        const getFirstRowQuery = `SELECT ${column.name} FROM ${keySpaceName}.${tableName} LIMIT 1`
        tableSchema["properties"][column.name] = await processColumn(column.name, column.type, getFirstRowQuery, definitionNames)
    }
    if (definitionNames.size !== 0)
        tableSchema['definitions'] = await getDefs(definitionNames)

    return tableSchema
}

async function getDefs(definitionNames) {
    const defs = {};
    const getUdtByName = `SELECT type_name, field_names, field_types FROM system_schema.types`
    try {
        for (const definitionName of definitionNames) {
            const udt = (await dbModule.execute(getUdtByName)).first()
            defs[definitionName] = await udtToSchema(udt)
        }
    } catch (err) {
        console.log(chalk.red(`\n[x] Can not get definitions for udt type`))
        console.log(`Error: ${err.message}\n`)
        throw err
    }

    return defs
}

async function processColumn(colName, column, getFirstRowQuery, definitionNames) {
    switch (column.type) {
        case 'ascii':
        case 'varchar':
        case 'uuid':
        case 'timeuuid':
        case 'duration':
        case 'inet':
            return ({type: "string"})
        case 'text':
            let textResultSet;
            try {
                textResultSet = await dbModule.execute(getFirstRowQuery)
            } catch (err) {
                return ({type: "string"})
            }
            if (textResultSet.rows.length === 0)
                return ({type: "string"})
            try {
                const value = textResultSet.first() ? textResultSet.first()[colName] : ""

                if (isNumeric(value))
                    return ({type: "string"})

                return objectToSchema(JSON.parse(value))
            } catch {
                return ({type: "string"})
            }
            break;
        case 'bigint':
        case 'counter':
        case 'decimal':
        case 'double':
        case 'float':
        case 'int':
        case 'varint':
        case 'smallint':
        case 'tinyint':
            return ({type: "number"})
        case 'boolean':
            return ({type: "boolean"})
        case 'timestamp':
            return ({type: "string", format: "date-time"})
        case 'date':
            return ({type: "string", format: "date"})
        case 'time mp':
            return ({type: "string", format: "time"})
        case 'blob':
            return ({type: "string", contentEncoding: "base64"})
        case 'list':
            return ({
                type: "array",
                uniqueItems: false,
                items: await processColumn(colName, column.children, getFirstRowQuery, definitionNames)
            })
        case 'set':
            return ({
                type: "array",
                uniqueItems: true,
                items: await processColumn(colName, column.children, getFirstRowQuery, definitionNames)
            })
        case 'map':
            return ({
                type: "object",
                additionalProperties: await processColumn(colName, column.children, getFirstRowQuery, definitionNames)
            })
        case 'tuple':
            const types = [];
            for (const type of column.children)
                types.push(await processColumn(colName, type, getFirstRowQuery, definitionNames))
            return ({
                type: "array",
                items: types
            })
        case "frozen":
            return await processColumn(colName, column.children, getFirstRowQuery, definitionNames)
        default:
            //UDT
            if (!Object.values(cassandraTypes).includes(column.type)) {
                definitionNames.add(column.type)
                return ({$ref: `#/definitions/${column.type}`})
            } else {
                return ({type: "undefined"})
            }
    }
}

async function udtToSchema(udt) {
    const schema = {type: "object", properties: {}}
    for (let i = 0; i < udt.field_names.length; i++) {
        const column = {name: udt.field_names[i], type: parseCassandraType(udt.field_types[i])}
        schema["properties"][column.name] = await processColumn(column.name, column.type)
    }
    return schema
}

function parseCassandraType(type) {
    let obj = {}
    const outerTypeEndIndex = type.indexOf('<');
    if (outerTypeEndIndex === -1) {
        obj['type'] = type
        obj['hasChildren'] = false
    } else {
        let outerType = type.slice(0, outerTypeEndIndex);
        obj['type'] = outerType
        obj['hasChildren'] = true
        if (outerType == 'map') {
            const keyValueTypes = type.slice(outerTypeEndIndex + 1, type.length - 1).split(', ')
            obj['children'] = parseCassandraType(keyValueTypes[1])
        } else if (outerType == 'tuple') {
            const membersType = type.slice(outerTypeEndIndex + 1, type.length - 1).split(', ')
            const types = [];
            for (const type of membersType)
                types.push(parseCassandraType(type))

            obj['children'] = types
        } else
            obj['children'] = parseCassandraType(type.slice(outerTypeEndIndex + 1, type.length - 1))
    }
    return obj
}

function objectToSchema(obj) {
    let schema = {type: "object", properties: {}}

    Object.keys(obj).forEach(k => {
        schema["properties"][k] = getItemType(obj[k])
    })

    return schema
}

function arrayObjectToSchema(array) {
    let schema = {type: "array"}

    if (array.length !== 0)
        schema["items"] = getItemType(array[0])
    return schema
}

function getItemType(item) {
    if (item === undefined || item === null)
        return ({type: "string"})
    switch (typeof item) {
        case 'boolean':
            return ({type: "boolean"})
        case 'string':
            return ({type: "string"})
        case 'number':
            return ({type: "number"})
        case 'object':
            if (Array.isArray(item))
                return arrayObjectToSchema(item)
            if (item instanceof Date)
                return ({type: "string", format: "date-time"})
            else
                return objectToSchema(item)
        default:
            return ({type: "string"})
    }
}

module.exports = {
    exportSchema
};
