const config = require('../config/config.js');
const cassandra = require('cassandra-driver');
const chalk = require('chalk')

const client = new cassandra.Client({
    contactPoints: [`${global.gDBConfig.host}:${global.gDBConfig.port}`],
    localDataCenter: `${global.gDBConfig.datacenter}`,
    credentials: {username: `${global.gDBConfig.user}`, password: `${global.gDBConfig.password}`}
});

async function connectToDB() {
    try {
        await client.connect()
        console.log(chalk.green(`\n[v] Connected to ${global.gDBConfig.host}:${global.gDBConfig.port}\n`))
    } catch (err) {
        console.log(chalk.red(`\n[x] Can not connect to ${global.gDBConfig.host}:${global.gDBConfig.port}`))
        console.log(`\tError: ${err.message}\n`)
        throw err;
    }
}

async function shutdown() {
    try {
        await client.shutdown()
        console.log(chalk.green(`\n[v] Disconnected from ${global.gDBConfig.host}:${global.gDBConfig.port}\n`))
    } catch (err) {
        console.log(chalk.red(`\n[x] Can not disconnect from ${global.gDBConfig.host}:${global.gDBConfig.port}`))
        console.log(`\tError: ${err.message}\n`)
        throw err;
    }
}

async function execute(query) {
        return await client.execute(query)
}

async function multiExecute(queries) {
    try {
        for (const query of queries) {
            await execute(query)
        }
        console.log(chalk.green(`\n[v] Keyspace and tables generated`))
    } catch (err) {
        console.log(chalk.red(`\n[x] Error running cql queries`))
        console.log(`\tError: ${err.message}\n`)
        throw err;
    }
}

function metadata() {
    return client.metadata
}


module.exports = {
    connectToDB,
    shutdown,
    metadata,
    execute,
    multiExecute,
    client
};
