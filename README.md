# Cassandra schema exporter

### Instalation

* Create a copy of _**config/config.example.json**_ in the same directory and name it as _**config.json**_

* Change config to valid db config values in **_config/config.json_**

* Run `npm install`

### Start

* Run `node index`

### Test

* Run `npm test`
